<?php
require_once "app/init.php";
if(!empty($_POST))
{
//    die(var_dump($_POST));
    $email = $_POST['email'];
    $user = $auth->getUserByEmail($email);
    if($user)
    {
        $token = $tokenHandler->createForgotPasswordToken($user->id);
        if($token)
        {
            $body = "<p>Use the below link within 15 mins to reset your password.</p>";
            $body .= "<p><a href='http://localhost:8088/reset-password.php?token={$token}&email={$email}'>RESET PASSWORD</a></p>";
        
            $mail->addAddress($user->email);
            $mail->Subject = "Reset Password";
            $mail->Body = $body;
            
            if($mail->send())
            {
                echo "Password reset link has been sent";
            }
            else
            {
                echo $mail->ErrorInfo;
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forgot Password</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
  <h1>Forgot Password</h1>
   <form action="" method="POST">
        <fieldset>
            <legend>Forgot Password</legend>
            <label>
                Email:
                <input type="text" name="email">
            </label>
            
            <input type="submit" value="Forgot Password">
        </fieldset>
    </form>
</body>
</html>