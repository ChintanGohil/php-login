<?php
session_start();
date_default_timezone_set('Asia/Kolkata');
$app = __DIR__;

require_once "{$app}/classes/Database.php";
require_once "{$app}/classes/Auth.php";
require_once "{$app}/classes/ErrorHandler.php";
require_once "{$app}/classes/Validator.php";
require_once "{$app}/classes/Hash.php";
require_once "{$app}/classes/TokenHandler.php";
require_once "{$app}/classes/MailConfigHelper.php";

$database = new Database();

/*
    Dependency Injection
*/
$hash = new Hash();
$auth = new Auth($database, $hash);
$errorHandler = new ErrorHandler();
$validator = new Validator($database, $errorHandler);
$tokenHandler = new TokenHandler($database, $hash);
$mail = MailConfigHelper::getMailer();

$auth->build();
$tokenHandler->build();

if(isset($_COOKIE['token']) && $tokenHandler->isValid($_COOKIE['token'],1))
{
    $user = $tokenHandler->getUserFromValidToken($_COOKIE['token']);
    $auth->setAuthSession($user->id);
}
