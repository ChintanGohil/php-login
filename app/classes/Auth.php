<?php

class Auth
{
    protected $database;
    protected $hash;

    protected $table = "users";
    protected $authSession = "user";

    /**
     * Auth constructor.
     * @param Database $database
     * @param Hash $hash
     */
    public function __construct(Database $database, Hash $hash)
    {
        $this->database = $database;
        $this->hash = $hash;
    }
    public function build()
    {
        $query = "CREATE TABLE IF NOT EXISTS {$this->table} (id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT, email VARCHAR (255) NOT NULL UNIQUE, username VARCHAR (255) NOT NULL UNIQUE, password VARCHAR (255) NOT NULL)";
        return $this->database->query($query);
    }
    public function create($data)
    {
        if(isset($data['password']))
        {
            $data['password'] = $this->hash->make($data['password']);
        }
        return $this->database->table($this->table)->insert($data);
    }
    public function signin($data): bool
    {
        $this->database->table($this->table)->where('username', '=', $data['username']);
        if($this->database->count() == 1)
        {
            $user = $this->database->first();
            if($this->hash->verify($data['password'], $user->password))
            {
                $this->setAuthSession($user->id);
                return true;
            }
        }
        return false;
    }
    
    public function setAuthSession($id)
    {
        $_SESSION[$this->authSession] = $id;
    }
    public function unsetAuthSession(){
        unset($_SESSION[$this->authSession]);
    }
    
    public function check()
    {
        return isset($_SESSION[$this->authSession]);
    }
    public function user()
    {
        if(!$this->check())
        {
            return false;
        }
        $user = $this->database->table($this->table)->where('id', '=', $_SESSION[$this->authSession])->first();
        return $user;
    }
    public function getUserByEmail(string $email)
    {
        return $this->database->table($this->table)->where("email", "=", $email)->first();
    }
    public function getUserByUsername(string $username)
    {
        return $this->database->table($this->table)->where("username", "=", $username)->first();
    }
    public function resetUserPassword(string $token, string $password)
    {
        $password = $this->hash->make($password);
        $sql = "UPDATE users, tokens SET users.password = '{$password}' WHERE users.id = tokens.user_id AND tokens.token = '{$token}'";
        return $this->database->query($sql);
    }
    public function signout()
    {
        setcookie('token', '', time()-5000);
        $user_id = $_SESSION[$this->authSession];
        $sql = "DELETE FROM tokens WHERE user_id = {$user_id} and is_remember = 1";
        $this->database->query($sql);
        $this->unsetAuthSession();
    }
}








?>