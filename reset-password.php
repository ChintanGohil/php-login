<?php
require_once "app/init.php";
if(!empty($_POST))
{
    $email = $_POST['email'];
    $token = $_POST['token'];
    $password = $_POST['password'];
    if($tokenHandler->isValid($token,0))
    {
        $password_reset_flag = $auth->resetUserPassword($token, $password);
        $token_delete_flag = $tokenHandler->deleteToken($token);
        if($password_reset_flag && $token_delete_flag)
        {
            header("Location: signin.php");
        }
        else
        {
            echo "Sorry, something issue while updating password";
        }
    }
    else
    {
        echo "Your time to reset the password has expired! Do not waste your time when you come here";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forgot Password</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
<?php
if(isset($_GET['token']) && isset($_GET['email'])):
    $token = $_GET['token'];
    $email = $_GET['email'];
    if($tokenHandler->isValid($token,0)):
?>
  <h1>Reset Password</h1>
   <form action="" method="POST">
        <fieldset>
            <legend>Reset Password</legend>
            
            <input type="text" name="token" value="<?= $token;?>">
            <label>
                Email:
                <!--AUTO FILLED-->
                <input type="text" name="email" value="<?= $email;?>">
            </label>
            <br>
            <label>
                New Password:
                <input type="password" name="password" >
            </label>
            
            <input type="submit" value="Reset my Password">
        </fieldset>
    </form>
<?php
    else:
        echo "token expired";
    endif;
else:
    echo "<p>how did you reach here??</p>";
endif;
?>
</body>
</html>