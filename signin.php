<?php
require_once "app/init.php";
if(!empty($_POST))
{
    $username = $_POST['username'];
    $password = $_POST['password'];
    $rememberMe = $_POST['remember_me'];

    $validator->check($_POST,[
        'username'=>[
            'required'=>true,
        ],
        'passwrod'=>[
            'required'=>true,
            'minlength'=>8
        ]
    ]);
    if($validator->fails())
    {
        echo "<pre>", print_r($validator->errors()->all()),"/pre>";
    
    }
    else{
        //log the user in
        $signin = $auth->signin([
            'username'=>$username,
            'password'=>$password
        ]);
        if($signin)
        {
            if($rememberMe)
            {
                $token = $tokenHandler->createRememberMeToken($auth->getUserByUsername($username)->id);
                setcookie('token', $token, time() + TokenHandler::REMEMBER_ME_EXPIRY_TIME_IN_SECONDS);
            }
            header("Location: index.php");
        }
        
        //create a session
        //remember me
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign In</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
  <h1>Sign In</h1>
   <form action="" method="POST">
        <fieldset>
            <legend>Sign In</legend>
            <label>
                Email:
                <input type="text" name="email">
            </label>
            <label>
                Username:
                <input type="text" name="username">
            </label>
            <label>
                Password:
                <input type="text" name="password">
            </label>
            <input type="checkbox" name="remember_me" checked="on">
            <input type="submit" value="sign-Up">
        </fieldset>
    </form>
</body>
</html>