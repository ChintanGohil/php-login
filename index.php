<?php
require_once "app/init.php";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
<a href="forgot-password.php">forgot password</a>
<?php if($auth->check()): ?>
    <p>You are signed in as "<?= $auth->user()->username; ?>"! <a href="signout.php">Sign Out</a> </p>
<?php else: ?>
    <p>You are not signed in <a href="signin.php">Sign In</a> OR <a href="signup.php">Sign Up</a> </p>
<?php endif; ?>
</body>
</html>